use std::io;
use std::net::UdpSocket;

fn main() -> std::io::Result<()> {
	loop {
		let socket = UdpSocket::bind("0.0.0.0:34255").expect("couldn't bind to address");
		socket
			.connect("192.168.1.67:34254")
			.expect("couldn't connect to address");
		// assert_eq!(
		// 	socket.peer_addr().unwrap(),
		// 	SocketAddr::V4(SocketAddrV4::new(Ipv4Addr::new(192, 168, 0, 1), 41203))
		// );
		let mut input = String::new();
		io::stdin().read_line(&mut input)?;
		if input.trim() == ":quit" {
			break;
		}
		let buf = input.trim().as_bytes();
		socket.send(&buf)?;
		let mut buf = [0; 10];
		socket.recv(&mut buf)?;
		println!("receive: {}", String::from_utf8_lossy(&buf));
	}
	Ok(())
}
